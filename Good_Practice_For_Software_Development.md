# Question 1
## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

1. According to the first section we have to be clear about our work whatever we have to do and after end of the work take the review of our work, And keep improving those areas where you think that is not ok for me right now.

2. In this section we have to be communicative with out team members, And do inform whatever you are doing . Try to reply all the answer whatever team member ask, In meeting keep turn on our video. It will improve the rapport with your team and help improve communication. 

3. At any point you are stuck explain the problem as easy as possible and mention the solution you tried out to fix the problem. Use screenshots, diagrams, screencats tools to explain. Look at the way issues get reported in large open-source projects.

4. Always try to intrect with team member,this will help a lot in improving your communication skill. Join the meetings 5-10 mins early to get some time with your team members.

5. It will not always be possible for your team members to get on a call with you. Instead, you can send a slack, Whatsapp message. But instead of bombarding them with many messages, you can write down all the questions you have and send it as a single message, becuse they have their own work to do as well. Pick and choose your communication medium depending on the situation.

6. Make sure you do some exercise to keep your energy levels high throughout the day.

# Question 2
## Which area do you think you need to improve on? What are your ideas to make progress in that area?

* Write code in well formated and readable way.
* Try to learn new technologies quickly.
* Try to be calm when working on big project, And learn from mistakes.
* You should strongly consider blocking social media sites and apps during work hours. Tools like TimeLimit, Freedom, or any other app can be helpful.
* While working on project when error is coming then try to resolve that as possible as you can.