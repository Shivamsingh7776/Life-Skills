# Tiny Habits
## Your takeaways from the video (Minimum 5 points):
1. We have to create any habbit that make us stay fit or calm like do push-up after doing something else and any celebration after completion of our work .
2. Clarify the aspiration.
3. Match with specific behaviours.
4. Find a good prompt.
5. Start tiny.


## our takeaways from the video in as much detail as possible.
Author discover universal formula for human behavior (B = MAP)
1. M = Motivation
2. Ability
3. Prompt

if you are developing a new habit, you will perform the habit if your motivation matches the ability required to read the moment you receive a prompt to read.
This whole video devided in to three part

### Part-1 Shrink the Behaviour
Author provided a chart to explain the relation between motivation and ability:
if a task is hard ,then you need high levels of motivation to rise above the action line and comlete the task and in  same way if task is easy , you need a little motivation to rise above action line and complete the task.Like brushing your teeth is easy to do It doesn’t matter if you're exhausted when you get the prompt to do it; you do it anyway.

### Part-2 Identify an Action Prompt
There are three types of habits prompt 
1. External: cues from your environment like post‐it notes, phone notifications, and alarms.
2. Internal: thoughts and sensations that remind you to act, like a grumbling stomach.
3. Action prompts: the completion of one behavior reminds you to start the next behavior. Loading the dishwasher can be a prompt to clean the kitchen countertops.

### Part-3 Grow Your Behavior with Some Shine:
According to the author- You know this feeling already: You feel Shine when you ace an exam. You feel Shine when you give a great presentation and
people clap at the end. You feel Shine when you smell something delicious that you cooked for the first time.When you feel successful at something, even if it’s tiny, your confidence grows quickly, and your motivation increases to do that habit
again and perform related behaviors. I call this success momentum. Surprisingly enough, this gets created by the frequency of your
successes, not by the size...You that you can resist learning to celebrate (small completions), but be aware that you’re choosing not to
be as good as you could be at creating habits. For most people, the effort of learning to celebrate is a small price to pay for becoming a
Habit Ninja.

## How can you use B = MAP to make making new habits easier?
* Get tools and resources that help me with the new habit.
* When we are going for any big task we can use this rule that make us more comfortable with any kind of condition.

## Why it is important to "Shine" or Celebrate after each successful completion of habit?
* Celebration is a thing that give us a relief and we  remember a lot because what kind of setuation came in front of us and how we tackle that.


## 1% Better Every Day Video

* Habits are compounded, thier effect are not that much in the starting but after a period of time we see a major improvement in ourself.
* If you want better results don't just set goals, instead focus on process of achieveing goal.
* The most effective way to change your habits is to focus not on what you want to achieve, but how to achieve.
* To make a good habit make it obvious, make it attractive, make it easy, and make it satisfying.
* Success is the result of daily habits - not one-time life changes. Making a decision in the day you are motivated does not make any  change because in the next day when you are not enough motivated you may forget about it.
* Getting 1 percent better doesn't mean to any numeric 1 percent but to get better by and small change every day.

## Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?
Reading book make peaple to consontrate on point they did not distract easily .

## Write about the book's perspective on making a bad habit more difficult?
Some time peaple are more adict towords books and they loss most of the time there.

















### Write about the book's perspective on how to make a good habit easier?

To make a good habit easier we need to make it:

1. Obvious
2. Attractive
3. Easy
4. Satisfying

To make good habit easier we have to put fewer steps between we and good behaviours.

Make habbits attractive.

Make it easy means reduce the friction and to prime our environment for the habits that we would like to develop. Reduce the friction makes it far more likely to do the thing.

We should try to attach some form of immediate gratification so that we can make habit immediately satisfying.




## Pick one habit that we would like to do more of? What are the steps that we can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

For so many days I was planning to solve leetcode questions every day in the morning after I woke up. So I want to make it a habit.
I will take a piece of paper or use word doc and make a list of our daily habits. One of the ways to change habits is to use a technique called pointing and calling.
To make it a habit I will assign a particular time slot when I am going to do it. I will make things interesting so that I can not procrastinate on that work. I will find easy ways to do that work. Finally, I will reward my work for the day.

3 Rs is one of the main ones:

-   Reminder. This is a trigger, or cue, that could be a conscious behavior
-   Routine. This is the behavior associated with the trigger. Doing something over and over can make the behavior routine.
-   Reward. The reward associated with behavior also helps make a habit stick. 
If we do something that causes enjoyment or relieves distress, the pleasurable release of dopamine in our brain can make us want to do it again.
