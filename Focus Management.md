
# Focus Management
## What is Deep Work?
Deep work is the ability to concentrate deeply on a difficult task for prolonged periods of time without getting distracted. It creates that intense, out-of-body kind of focus that makes you completely oblivious to what’s going on around you – the kind that produces your best work.
