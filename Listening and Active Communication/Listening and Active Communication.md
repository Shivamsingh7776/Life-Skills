# Active Linstening
## What are the steps/strategies to do Active Listening?
* Focus on the speaker and topics whatever he is saying.
* Avoid getting distracted by your own thoughts.
* Try not to interrupt the other person, let them finish and then respond.
* Show that you listening with body language like react with conversation to show yourself i am listenning actively.
* If some important conversation is going on i am not able to get that then i can take a notbook and write down the key point for further understanding.
* When you are going for any kind of conversation then you have to understand fully whatever someone is saying.

## According to Fisher's model, what are the key points of Reflective Listening?
Listening is following the thoughts and feelings of another and understanding what the other is
saying from his or her perspective. Reflective listening is a special type of listening that involves
paying respectful attention to the content and feeling expressed in another persons 
communication. Reflective listening is hearing and understanding, and then letting the other
know that he or she is being heard and understood. It requires responding actively to another
while keeping your attention focused completely on the speaker. In reflective listening, you do
not offer your perspective by carefully keep the focus on the other’s need or problem. Thus
reflective listening consists of a step beyond what is normally thought of as listening:
1. Responding to the other person by reflecting the thoughts and feelings you heard in his or
her words, tone of voice, body posture, and gestures.
2. Hearing and understanding what the other person is communicating through words and
body language to the best of your ability

## What are the obstacles in your listening process?
* When i am in conversation with someone at that time some Irrelevant ideas come in my mind so that i got destructed.
* I am not able to maintain the concentration on that topic for long time, When speaker takes more time then i started loosing interest on that topic.
* Talking with friends in between the concentration that is also obstacles of active listening 

## What can you do to improve your listening?
* Seat with unknown person to avoid the disturbance during the whole season and do not loose inerest on that topic whatever speaker is speaking just listen carefully and take a notbook for noting some point that i am not able understand, For more interaction ask some question from speaker if he is interested for
reply some point that you are not able to understand.

## When do you switch to Passive communication style in your day to day life?
Passive communication is not standing up for self.
* Example1 :- In office someone keep asking project question again and again at that time you are not able say sorry i do not know the answer.
* Example2:- When I am going to purchase something i am not satisfied with the product but still i take that.

## When do you switch into Aggressive communication styles in your day to day life?
Aggressive communication is standing up for self and not respecting the other person-
* When someone is talking with disrespectfully at that time may be i switch to aggressive communication, Else i am calm person so i avoid that kind of situation.
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
In case of passive aggressive communication showing sarcasm or talking behind someone back.
* Example:- when i am private discussion at that time, Most of the time this happens with my friends.
## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)
* Learn to recognize and name you feelings and need.
* Be aware of your body what body demands.
* Do not wait for something you need.


